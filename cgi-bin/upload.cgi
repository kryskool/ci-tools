#!/usr/bin/env python3

import cgi
import cgitb
import sys
import os
import hashlib

cgitb.enable()
sha256_hash = hashlib.sha256()


print("Content-Type: text/plain")

if os.environ.get('REQUEST_METHOD', 'GET') == "GET":
    print("Status: 404")
    print("")
    print("404: NOT FOUND")
    sys.exit(0)

print("")

form = cgi.FieldStorage()

if 'token' not in form:
    print("Status: 401")
    print("")
    print("401: Unauthorized")
    print("No TOKEN found, exit !!!")
    sys.exit(0)

if os.environ.get('AUTH_TOKEN') != form['token'].value:
    print("Status: 403")
    print("")
    print("403: Forbidden")
    print("Bad TOKEN, exit !!!")
    sys.exit(0)

fileitem = form['filename']
print("Upload file %s..." % fileitem.filename)

# strip leading path from file name to avoid
# directory traversal attacks
fn = os.path.basename(fileitem.filename)

# Read and write buffer with 100K blocs
with open(fn, 'wb') as ftmp:
    while True:
        chunk = fileitem.file.read(100000)
        if not chunk:
            break
        ftmp.write (chunk)

print('The file "' + fn + '" was uploaded successfully')

# Read and update hash string value in blocks of 4K
with open(fn,"rb") as f:
    for byte_block in iter(lambda: f.read(4096), b""):
        sha256_hash.update(byte_block)

print("SHA256 Checksum: " + sha256_hash.hexdigest())

sys.exit(0)